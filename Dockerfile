# check https://hub.docker.com/r/chocobozzz/peertube/tags
# for chosing which versoin you want to run with
FROM chocobozzz/peertube:production-bullseye

# USER peertube

# RUN npm run setup:cli
# RUN echo 'alias peertube="cd /app && node ./dist/server/tools/peertube.js"' >> /home/peertube/.bashrc

RUN mkdir -p /var/www/peertube/storage
VOLUME /var/www/peertube/storage

COPY ./entrypoint.sh /usr/local/bin/entrypoint.sh
ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]

EXPOSE 9000 1935 

CMD ["node", "--inspect=0.0.0.0", "dist/server"]
