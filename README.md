Peertube Plugin Development Toolkit
=================================================

> WARNING:  
> This is a work in progres, use at your own risk

This repo contains an adaptation of the tutorial described on
https://framacolibri.org/t/peertube-pluginn-dev/17631 by
John Livingston, adapted to try to streamline it.

Goals
--------

This repo is intended to spawn peertube instances using docker
for the sake of plugin development. Opinionated choices driving
this toolkit are:

- one should test their plugin against verious version of peertube
  (stable, development, old versions) and using docker makes it easy.

- plugin developers should take in account federation, and in some cases
  check how things go when plugin is installed on 2 different peertubes.
  Many plugins are local-peertube only but, by setting up this toolkit,
  we hope to see more fediverse-aware peertube plugins (this need was
  initially driven by peertube-plugin-livechat).

- it is frequent that plugin developers have several plugins on their
  workbench, and it should be easy to test them in combination.

Quick start
------------

    cp docker-compose-default.yml docker-compose.yml
    docker-compose up

Then you can access:

- http://peertube.localhost:9000
- http://peertube-other.localhost:9001
- http://mail.localhost:8025

As we are in dev env, defaut user for peertube
is `root` with `test` password.

A word about some constraints
-------------------------------

We found it hard to setup federation in this configuration.
The only easy way was to name the container with the dev FQDN,
so that inside docker both instances could communicate without
getting out, as *.localhost would resolve to .. localhost.

Also if you use debian-bullseye, don't use the outdated `docker.io`
package. Follow instructions on 
https://docs.docker.com/engine/install/debian/ for an up to date version
(you will fail if you try to use this toolkit on old version).

Federating both istances
-------------------------

On http://peertube.localhost:9000/admin/follows/following-list
you can add the following of `peertube-other.localhost:9001`
and vice versa.

Note: do not worry about the warning that says you need https,
this message don't apply as we are in dev mode (we gotta PR the
heck out of that one soon).

Custom env
------------

You can create custom env files, just name them `env.local.*` 
this pattern is included in `.gitignore`. Then you can point to
it in your `docker-compose.yml` file.

Access containers
-------------------

    docker-compose exec peertube.localhost bash

Install or refresh plugin
----------------------------

    # install plugin
    docker-compose exec -u peertube peertube.localhost \
      npm run plugin:install -- \
      --plugin-path /peertube-plugin-livechat

    # uninstall plugin
    docker-compose exec -u peertube peertube.localhost \
      npm run plugin:uninstall -- \
      --npm-name peertube-plugin-livechat
    
    docker-compose exec -u peertube peertube.localhost \
      rm -rf /home/peertube/.cache/yarn

You need to restart container after installing or uninstalling a plugin:

    docker-compose restart peertube.localhost

Note: it's possible to install/uninstall plugins without restarting using
the wrapper described in 
https://docs.joinpeertube.org/maintain/tools#peertube-plugins-js
but then you need to `npm run setup:cli` first.


Using node debugger with codium
----------------------------------

Add a file `~/.vscode/launch.json`

    {
      "configurations": [
        {
          "type": "node",
          "name": "peertube.localhost",
          "request": "attach",
          "address": "peertube.localhost",
          "port": 9229,
          "localRoot": "${workspaceFolder}",
          "remoteRoot": "/var/www/peertube/storage/plugins/node_modules/peertube-plugin-livechat/"
        }
      ]
    }

Note: This will watch the first peertube, you can switch the name and prot to get the second (`peertube-other.localhost` and port `9230`).

Then you can place breakpoints in your codium and use the `Debug` menu.

Todo
--------

- include setup-cli and plugin install in `entrypoint.sh`

Authors
--------

- mose
- John Livingston

License
--------

![CC0](cc0.png)

CC0 - This code is public domain.  
https://creativecommons.org/share-your-work/public-domain/cc0/

Any contribution will comply with this refusal
of the concept of copyright.
