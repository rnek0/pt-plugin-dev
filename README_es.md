Kit de herramientas de desarrollo de Plugins Peertube
=====================================================

> ADVERTENCIA:  
> Este trabajo esta en evolución constante, úselo bajo su propria responsabilidad.

Este repositorio contiene una adaptación del tutorial compartido en <https://framacolibri.org/t/peertube-pluginn-dev/17631> por John Livingston, siendo adaptado para intentar simplificarlo.


Objetivos
---------

Este repositorio está destinado a levantar instancias de Peertube usando docker para favorecer el desarrollo de Plugins. 

Se asumen estos requisitos necesarios para el desarrollo con el kit :

- los desarrolladores de plugins deberán tomar en cuenta la federación, y en algunos casos comprobar el comportamiento al instalar el plugin en 2 instancias diferentes de Peertube.

- La mayoría de plugins fueron pensados para funcionar en una sola instancia, confiamos al desarrollar este toolkit poder favorecer la aparición de plugins que tomen en cuenta la federación (inicialmente esta necesidad fue impulsada por el plugin livechat ).

- es frecuente que los desarrolladores tengan varios plugins en su banco de trabajo, con este kit debería ser fácil establecer tests que les permitan comunicar entre ellos.

Inicio rápido
-------------

Entre en la terminal :

```bash
    cp docker-compose-default.yml docker-compose.yml
    docker-compose up
```

Puede acceder a las instancias con :

- http://peertube.localhost:9000
- http://peertube-other.localhost:9001
- http://mail.localhost:8025

Estamos en un entorno de desarrollo, las credenciales para las instancias Peertube son `root` con el pass `test` .

Advertencia sobre algunas limitaciones
--------------------------------------

Nos resultó difícil configurar la federación de esta forma.  
La única via sencilla fué nombrar el contenedor con el FQDN del desarrollador, de tal manera que dentro de Docker ambas instancias pudieran comunicarse entre si sin estar expuestas à la red, ya que *.localhost debía tener la misma resolución de nombre que localhost.


Además, si usa Debian bullseye, utilize el paquete `docker.io` actualizado. Para ello siga las instrucciones en https://docs.docker.com/engine/install/debian/ (fallará si intenta usar este kit de herramientas con una versión anterior).

Federando ambas instancias
--------------------------

A http://peertube.localhost:9000/admin/follows/following-list le puede añadir peertube-other.localhost:9001 y vice versa.


Nota: no se preocupe por la advertencia sobre la necesidad https, este mensaje no aplica ya que estamos en modo desarrollador (en breve haremos un PR para ello).

Entorno personalizado
---------------------

Puede crear archivos env personalizados, simplemente se debe asignar el nombre `env.local.*`.  
Este patrón está incluido en el `.gitignore`. Asi puede apuntar hacia su archivo personalizado en su archivo docker-compose.yml`.

Acceder a los contenedores
--------------------------

    docker-compose exec peertube.localhost bash

Instalar o poner al dia el plugin
---------------------------------

    # install plugin
    docker-compose exec -u peertube peertube.localhost \
      npm run plugin:install -- \
      --plugin-path /peertube-plugin-livechat

    # uninstall plugin
    docker-compose exec -u peertube peertube.localhost \
      npm run plugin:uninstall -- \
      --npm-name peertube-plugin-livechat
    
    docker-compose exec -u peertube peertube.localhost \
      rm -rf /home/peertube/.cache/yarn

Debe reiniciar el contenedor después de instalar o desinstalar un plugin:

    docker-compose restart peertube.localhost

Nota: es posible instalar/desinstalar plugins sin reiniciar utilizando el wrapper descrito en https://docs.joinpeertube.org/maintain/tools#peertube-plugins-js para ello necesita antes lanzar `npm run setup:cli` .


Para lanzar el depurador node con codium
----------------------------------------

Añada lo siguiente al archivo `~/.vscode/launch.json`

    {
      "configurations": [
        {
          "type": "node",
          "name": "peertube 1 localhost",
          "request": "attach",
          "address": "peertube.localhost",
          "port": 9229,
          "localRoot": "${workspaceFolder}",
          "remoteRoot": "/var/www/peertube/storage/plugins/node_modules/peertube-plugin-livechat/"
        },
        {
          "type": "node",
          "name": "peertube 2 localhost",
          "request": "attach",
          "address": "peertube-other.localhost",
          "port": 9230,
          "localRoot": "${workspaceFolder}",
          "remoteRoot": "/var/www/peertube/storage/plugins/node_modules/peertube-plugin-livechat/"
        }
      ]
    }

Nota: Esto contempla la primera instancia peertube, puede cambiar el "name" y el "port" para depurar la segunda (`peertube-other.localhost` con el puerto `9230`).

Una vez configurado su archivo .json ya puede poner sus puntos de interrupción en el código y usar el menú 'Depurar'.

Todo
----

- include setup-cli and plugin install in entrypoint.sh

Autores
-------

- mose
- John Livingston

Licencia
--------

![CC0](cc0.png)

CC0: este código es de dominio público.
https://creativecommons.org/share-your-work/public-domain/cc0/

Toda contribución se ajustará a este rechazo del concepto de derechos de autor.
